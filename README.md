Mom's Custom Tattoo & Body Piercing of Spokane WA. We have a passion for safe piercing & one-of-a-kind, custom tattoos. We also offer unique & stylish implant grade jewelry for all shapes and sizes! We believe all bodies are amazing and you should adorn yours how you'd like.

Website: https://www.momsjewelry.com/
